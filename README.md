# solution-FitzHugh-Nagumo

The lab task is to implement a solution for FitzHugh-Nagumo system of equations:
* https://en.wikipedia.org/wiki/FitzHugh%E2%80%93Nagumo_model
* https://en.wikipedia.org/wiki/Reaction%E2%80%93diffusion_system) using Chebyshev's spectral methods.

For the initial condition, it's recommended to use a slightly perturbed field with the average value around 0.5

An example of coefficients is present on the attached picture (it's similar to what was used to generate the picture on the Wikipedia page), but you can use any other set of parameters as long as it results in a non-trivial behaviour (oscillations etc.)

All of materials is contained in the materials folder

-------------------------------------------------------------------------------------------------------------

Задание состоит в том, чтобы решить вариацию системы уравнений ФитцХью-Нагумо
 * https://en.wikipedia.org/wiki/FitzHugh%E2%80%93Nagumo_model
 * https://en.wikipedia.org/wiki/Reaction%E2%80%93diffusion_system
с помощью спектральных методов и полиномов Чебышёва.

В качестве начальных условий рекомендуется взять немного возмущённое поле со средним значением около 0.5

Пример коэффициентов есть на картинке в приложении (с ними должна получиться картинка из Википедии или что-то похожее), но вместо них можно использовать какой угодно другой набор, который даёт нетривиальное поведение (осцилляции и т.п.)
