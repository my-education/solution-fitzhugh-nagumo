import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
import numpy as np


def to_zero(vv, v=0.0):
	vv[:, 0] = v
	vv[:, N] = v
	vv[0, :] = v
	vv[N, :] = v
	return vv


# System of equation wikipedia, where
# u is the membrane potential,
# W is a recovery variable,
# I is the magnitude of stimulus current.
# u' = u - u ** 3 / 3 - w + Iext
# tay_w' = u + a - b*w

# one more description
# V˙ = V − V ** 3 / 3 - W + I
# W˙= 0.08 (V + 0.7 − 0.8W)

# finally description:
# u/dt = u'' * Du - k + sigma*u - u**3 - sigma*v
# v/dt = v''* Dv + u - v

# Coefficients, where y = sigma
tau, sigma, Du, Dv, k = 0.1, 1, 0.0028, 0.005, -0.05
N = 24


def cheb_1d(v):
	N = len(v)-1
	x  = np.cos(np.pi * np.arange(0, N + 1) / N)  # express Chebyshev points on [-1, 1] through equidistant points on [0..pi]
	ii = np.arange(0, N)
	V = np.flipud(v[1:N])
	V = list(v) + list(V)  # values for the 2nd half of the circle
	U = np.real(np.fft.fft(V))
	b = list(ii)
	b.append(0)
	b = b + list(np.arange(1-N, 0))  # fftfreq array
	w_hat = 1j * np.array(b)  # expression for 1st order derivative for Fourier space
	w_hat = w_hat * U   # applying the derivative
	W = np.real(np.fft.ifft(w_hat))  # transform the derivative back onto the circle in np.real space
	w = np.zeros(N+1)

	w[1:N] = -W[1:N] / np.sqrt(1 - x[1:N] ** 2)  # derivative on (-1, 1) from derivative on (0..pi)
	w[0] = sum(ii**2*U[ii])/N + 0.5*N*U[N]  # special expressions for the edges
	w[N] = sum((-1) ** (ii + 1) * ii ** 2 * U[ii]) / N + 0.5 * (-1) ** (N + 1) * N * U[N]
	return w


# t нужен изначальный или промежуточный?
def chebfft(x, y, vv):
	uxx = np.zeros((N + 1, N + 1))
	uyy = np.zeros((N + 1, N + 1))
	ii = np.arange(1, N)
	for i in range(1, N):  # 2nd order derivative using Chebyshev space and FFT
		v = vv[i, :]
		V = list(v) + list(np.flipud(v[ii]))
		U = np.real(np.fft.fft(V))
		w1_hat = 1j * np.zeros(2 * N)
		w1_hat[0:N] = 1j * np.arange(0, N)
		w1_hat[N + 1:] = 1j * np.arange(-N + 1, 0)
		W1 = np.real(np.fft.ifft(w1_hat * U))
		w2_hat = 1j * np.zeros(2 * N)
		w2_hat[0:N + 1] = np.arange(0, N + 1)
		w2_hat[N + 1:] = np.arange(-N + 1, 0)
		W2 = np.real(np.fft.ifft((-w2_hat ** 2) * U))
		# note that uxx (and uyy) remain 0 for the boundaries
		uxx[i, ii] = W2[ii] / (1 - x[ii] ** 2) - (x[ii] * W1[ii]) / (1 - x[ii] ** 2) ** (3.0 / 2)
	for j in range(1, N):  # same for Y axis
		v = vv[:, j]
		V = list(v) + list(np.flipud(v[ii]))
		U = np.real(np.fft.fft(V))
		w1_hat = 1j * np.zeros(2 * N)
		w1_hat[0:N] = 1j * np.arange(0, N)
		w1_hat[N + 1:] = 1j * np.arange(-N + 1, 0)
		W1 = np.real(np.fft.ifft(w1_hat * U))
		w2_hat = 1j * np.zeros(2 * N)
		w2_hat[0:N + 1] = np.arange(0, N + 1)
		w2_hat[N + 1:] = np.arange(-N + 1, 0)
		W2 = np.real(np.fft.ifft(-(w2_hat ** 2) * U))
		uyy[ii, j] = W2[ii] / (1 - y[ii] ** 2) - y[ii] * W1[ii] / (1 - y[ii] ** 2) ** (3.0 / 2.0)

	return uxx, uyy  # вторая производная по пространсту


dt = 3.0 / (N**2)
max_t = 2.0
max_iter = int(round(max_t / dt) / 2)

# initial states
x = np.cos(np.pi * np.arange(0, N + 1) / N)
y = np.cos(np.pi * np.arange(0, N + 1) / N)

# generate x and y for plot
x_plot, y_plot = np.meshgrid(x, y)
u0, v0 = [], []
for i in range(0, N + 1):
	u0.append(np.array(np.random.uniform(0.3, 0.6, N + 1)))
	v0.append(np.array(np.random.uniform(0.3, 0.6, N + 1)))
u0, v0 = np.array(u0), np.array(v0)
u0 = to_zero(u0, 0.4)
v0 = to_zero(v0, 0.4)
# u0 = 0.5*np.cos(x_plot*(np.pi/2)) + 0.1*np.sin(y_plot*(np.pi/2))
# v0 = 0.2*np.cos(x_plot*(np.pi/2)) + 0.9*np.sin(y_plot*(np.pi/2))
data_u = np.zeros(shape=(max_iter, N + 1, N + 1))
data_v = np.zeros(shape=(max_iter, N + 1, N + 1))
data_u[0] = u0
data_v[0] = v0
vu_old = u0
vv_old = v0

for n in range(1, max_iter):
	uxx = np.zeros((N + 1, N + 1))
	uyy = np.zeros((N + 1, N + 1))
	ii = np.arange(1, N)

	w2ux, w2uy = chebfft(x, y, vu_old)  # return 2 производная
	w2vx, w2vy = chebfft(x, y, vv_old)  # return 2 производная

	# vu_new = dt * ((w2ux + w2uy) * Du - k + sigma * u0 - u0 ** 3 - sigma * v0)
	# vv_new = dt * (((w2vx + w2vy) * Dv + u0 - v0) / tau)

	vu_new = dt * ((w2ux + w2uy) * Du - k + sigma * u0 - u0 ** 3 - 0.8 * v0) + vu_old
	vu_new = to_zero(vu_new, 0.4)
	vv_new = dt * (((w2vx + w2vy) * Dv + u0 - v0) / 1.1) + vv_old
	vv_new = to_zero(vv_new, 0.4)
	data_u[n] = vu_new
	data_v[n] = vv_new

	vu_old = vu_new
	vv_old = vv_new


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111, projection='3d')


def animate(i):
	print(i)
	ax.clear()
	ax.set_zlim([0, 1.1])
	ax.plot_surface(x_plot, y_plot, data_u[i], cmap='coolwarm', linewidth=0, rstride=2, cstride=2)
	# ax.plot_surface(xv, yv, v[i], cmap='coolwarm', linewidth=0, rstride=2, cstride=2)

	ax.set_title('%03d' % (i))
	return ax


ani = animation.FuncAnimation(fig, animate, np.arange(0, max_iter, 10), interval=1, blit=False, repeat_delay=100)
# you can use interp2d to plot prettier plots

plt.show()
